(function ($) {

  "use strict";

  Drupal.plupload_s3 = {
    fileUploaded: function(up, file, result) {
      var $this = $(up.settings.container);
      var $form = $this.parents('form');
      var $fileField = $($form).find('input.uploader-files-element');
      var $files = JSON.parse($fileField.val());
      $files.push(file);
      $fileField.val(JSON.stringify($files));
    },
    isChunkable: function ( fileSize ) {
      var KB = 1024;
      var MB = ( KB * 1024 );
      var minSize = ( MB * 5 );
      return( fileSize > minSize );
    },
    beforeUpload: function (uploader, file) {
      file.isChunked = Drupal.plupload_s3.isChunkable(file.size);
      file.s3Key = ( "pluploads/" + file.id + "/" + file.name );
      if (file.isChunked) {
        file.chunkIndex = 0;
        // Create the chunk-based S3 resource by appending the chunk index.
        file.chunkKey = (file.s3Key + "." + file.chunkIndex);
        // Define the chunk size - this is what tells Plupload that the file
        // should be chunked. In this case, we are using 5MB because anything
        // smaller will be rejected by S3 later when we try to combine them.
        // --
        // NOTE: Once the Plupload settings are defined, we can't just use the
        // specialized size values - we actually have to pass in the parsed
        // value (which is just the byte-size of the chunk).
        uploader.settings.chunk_size = plupload.parseSize("5mb");
        // Since we're chunking the file, Plupload will take care of the
        // chunking. As such, delete any artifacts from our non-chunked
        // uploads (see ELSE statement).
        delete(uploader.settings.multipart_params.chunks);
        delete(uploader.settings.multipart_params.chunk);
        // Update the Key and Filename so that Amazon S3 will store the
        // CHUNK resource at the correct location.
        uploader.settings.multipart_params.key = file.chunkKey;
        uploader.settings.multipart_params.Filename = file.chunkKey;
        // This file CANNOT be chunked on S3 - it's not large enough for S3's
        // multi-upload resource constraints
      } else {
        // Remove the chunk size from the settings - this is what tells
        // Plupload that this file should NOT be chunked (ie, that it should
        // be uploaded as a single POST).
        uploader.settings.chunk_size = 0;
        // That said, in order to keep with the generated S3 policy, we still
        // need to have the chunk "keys" in the POST. As such, we'll append
        // them as additional multi-part parameters.
        uploader.settings.multipart_params.chunks = 0;
        uploader.settings.multipart_params.chunk = 0;
        // Update the Key and Filename so that Amazon S3 will store the
        // base resource at the correct location.
        uploader.settings.multipart_params.key = file.s3Key;
        uploader.settings.multipart_params.filename = file.s3Key;
      }
    },
    chunkUploaded: function (uploader, file, info) {
      if (typeof file.chunks === 'undefined') {
        file.chunks = [];
      }
      file.chunks.push(file.chunkKey);
      file.chunkKey = ( file.s3Key + "." + ++file.chunkIndex );
      uploader.settings.multipart_params.key = file.chunkKey;
      uploader.settings.multipart_params.Filename = file.chunkKey;
    }
  };

})(jQuery);
