# Plupload S3 integration (with chunking.)

## Library compatibility

`drupal/plupload` module and the code contained in this module are compatible
with Plupload < 3; this should be addressed at some point for Plupload 3 but
has not happened yet. Contributions to both projects are welcome.

## Max upload size

Reminder, if you are using this library you likely intend for users to upload
files in excess of PHP/Drupal's max sizes, either as set in an `ini` option or
on a field widget's config. At the moment, the field widget provided here
_does not_ sidestep validation for max upload size, and so your settings must
accomodate for these larger files, else the upload will be rejected on save
during entity validation.

&copy; Brad Jones LLC, Fruition Growth LLC and contributors.
