<?php declare(strict_types=1);

namespace Drupal\plupload_s3;

use Aws\S3\PostObjectV4;
use Aws\S3\S3ClientInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;

/**
 * Abstractions for handling plupload file Form API elements and value
 * processing. This lives here such that it can be accessed by both field
 * widget plugins and custom form implementations.
 */
class PluploadElementProcessor {

  /**
   * File system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructor.
   */
  public function __construct(FileSystemInterface $fileSystem) {
    $this->fileSystem = $fileSystem;
  }

  /**
   * Process an element to add chunked upload keys/arrays.
   *
   * @param array $element
   *   Render element to adjust.
   * @param string[] $allowedExtensions
   *   Array of allowed extensions.
   * @param \Aws\S3\S3ClientInterface $client
   *   S3 client.
   * @param string $bucket
   *   S3 bucket.
   *
   * @return array
   *   The element with uploader and files Form API arrays added,
   *   and the JS library added to the attachments.
   */
  static function processElement(array &$element, array $allowedExtensions, S3ClientInterface $client, string $bucket) {
    $conditions = [
      ['bucket' => $bucket],
      ['starts-with', '$name', ''],
      ['starts-with', '$chunk', ''],
      ['starts-with', '$chunks', ''],
      ['starts-with', '$key', ''],
      ['starts-with', '$filename', ''],
    ];
    $postObject = new PostObjectV4($client, $bucket, [], $conditions, '+12 hours');
    $attributes = $postObject->getFormAttributes();
    $inputs = $postObject->getFormInputs();
    $element['uploader'] = [
      '#type' => 'plupload',
      '#title' => t('Upload to S3'),
      // This doesn't actually do anything but changes the help text.
      '#upload_validators' => [
        'file_validate_extensions' => [implode(' ', $allowedExtensions)],
      ],
      '#plupload_settings' => [
        'runtimes' => 'html5,html4',
        'url' => $attributes['action'],
        'chunk_size' => '5mb',
        'max_file_size' => '0',
        'unique_names' => TRUE,
        'multipart' => TRUE,
        'multipart_params' => (object) $inputs,
        'filters' => (object) [
          'mime_types' => [
            (object) [
              'title' => 'Allowed filetypes',
              'extensions' => implode(',', $allowedExtensions),
            ],
          ],
        ],
      ],
      '#event_callbacks' => [
        // @todo - Callback for file add to handle max items enforcement.
        'BeforeUpload' => 'Drupal.plupload_s3.beforeUpload',
        'ChunkUploaded' => 'Drupal.plupload_s3.chunkUploaded',
        'FileUploaded' => 'Drupal.plupload_s3.fileUploaded',
      ],
    ];
    $element['#attached']['library'][] = 'plupload_s3/s3-uploads';
    $element['files'] = [
      '#type' => 'hidden',
      '#default_value' => '[]',
      '#attributes' => [
        'class' => ['uploader-files-element'],
      ],
    ];
    return $element;
  }

  /**
   * Complete chunked upload.
   *
   * @param \stdClass $file
   *   File data object from plupload payload.
   * @param string $destinationPrefix
   *   Destination URI with optional path prefix, e.g. scheme://[prefix]
   * @param \Aws\S3\S3ClientInterface $client
   *   S3 client.
   * @param string $bucket
   *   S3 bucket to utilize.
   *
   * @return string
   *   Saved file URI.
   */
  protected function completeChunkedUpload(\stdClass $file, string $destinationPrefix, S3ClientInterface $client, string $bucket) {
    $realPath = $this->fileSystem->getDestinationFilename(
      $destinationPrefix . basename($file->s3Key),
      FileSystemInterface::EXISTS_RENAME
    );
    $realname = str_replace(
      StreamWrapperManager::getScheme($destinationPrefix) . '://',
      '',
      $realPath
    );
    $model = $client->createMultipartUpload([
      'Bucket' => $bucket,
      'Key' => $realname,
    ]);
    $uploadId = $model->get('UploadId');
    $s3parts = [];
    $counter = 1;
    foreach ($file->chunks as $chunk) {
      $result = $client->uploadPartCopy([
        'Bucket' => $bucket,
        'CopySource' => rawurlencode($bucket . '/' . $chunk),
        'Key' => $realname,
        'PartNumber' => $counter,
        'UploadId' => $uploadId,
      ]);
      $s3parts[] = [
        'ETag' => $result->get('CopyPartResult')['ETag'],
        'PartNumber' => $counter,
      ];
      $counter++;
    }
    $client->completeMultipartUpload([
      'Bucket' => $bucket,
      'Key' => $realname,
      'UploadId' => $uploadId,
      'MultipartUpload' => [
        'Parts' => $s3parts,
      ],
    ]);
    return $realPath;
  }

  /**
   * Process posted Plupload files.
   *
   * Important: This assumes files/chunks are uploaded to the same bucket as
   * they will be stored at, permanently. S3 supports splitting this horizon,
   * but it is beyond the scope of this implementation.
   *
   * @param string $json
   *   Posted JSON string from Plupload.
   * @param string $destinationPrefix
   *   Destination prefix, in form of scheme://[prefix]
   * @param \Aws\S3\S3ClientInterface $client
   *   S3 client.
   * @param string $bucket
   *   Bucket.
   *
   * @return string[]
   *   Arrays of completed uploaded file URIs.
   */
  public function processPostedFiles(string $json, string $destinationPrefix, S3ClientInterface $client, string $bucket) {
    try {
      $files = \GuzzleHttp\json_decode($json);
    }
    catch (\InvalidArgumentException $e) {
      // @todo - handle and warn.
      return [];
    }
    $uploaded = [];
    foreach ($files as $file) {
      $source = $file->s3Key;
      if ($file->isChunked) {
        $source = $this->completeChunkedUpload(
          $file,
          $destinationPrefix,
          $client,
          $bucket
        );
      }
      else {
        // Move from temporary name to permanent location.
        $dest = $destinationPrefix . basename($source);
        try {
          $this->fileSystem->move(
            StreamWrapperManager::getScheme($destinationPrefix) . '://' . $source,
            $dest
          );
        }
        catch (FileException $e) {
          throw $e;
        }
        $source = $dest;
      }
      $uploaded[] = $source;
    }
    return $uploaded;
  }

}
