<?php declare(strict_types=1);

namespace Drupal\plupload_s3\Plugin\Field\FieldWidget;

use Aws\Credentials\Credentials;
use Aws\Exception\CredentialsException;
use Aws\S3\PostObjectV4;
use Aws\S3\S3Client;
use Aws\S3\S3ClientInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\Entity\File;
use Drupal\key\KeyRepositoryInterface;
use Drupal\plupload_s3\PluploadElementProcessor;
use GuzzleHttp\Promise\RejectedPromise;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function GuzzleHttp\Promise\promise_for;

/**
 * Plugin implementation of the 'plupload_s3_file' widget.
 *
 * @FieldWidget(
 *   id = "plupload_s3_file",
 *   label = @Translation("Plupload S3"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class S3UploadFileWidget extends WidgetBase {

  /**
   * The key repository service.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Flysystem Factory; optional.
   *
   * @var \Drupal\flysystem\FlysystemFactory|null
   */
  protected $flySystemFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Plupload Element processor.
   *
   * @var \Drupal\plupload_s3\PluploadElementProcessor
   */
  protected $elementProcessor;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    KeyRepositoryInterface $key_repository,
    ModuleHandlerInterface $module_handler,
    ?object $flysystem_factory,
    AccountInterface $current_user,
    PluploadElementProcessor $element_processor
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->keyRepository = $key_repository;
    $this->moduleHandler = $module_handler;
    if ($flysystem_factory) {
      $this->flySystemFactory = $flysystem_factory;
    }
    $this->currentUser = $current_user;
    $this->elementProcessor = $element_processor;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('key.repository'),
      $container->get('module_handler'),
      $container->get('flysystem_factory', ContainerInterface::NULL_ON_INVALID_REFERENCE),
      $container->get('current_user'),
      $container->get('plupload_s3.element_processor')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $options = $this->keyRepository
      ->getKeyNamesAsOptions(['type' => 'authentication_multivalue']);
    $form = [
      'key_name' => [
        '#type' => 'select',
        '#title' => $this->t('AWS key for upload URL signing'),
        '#options' => $options,
        '#description' => $this->t('Multivalue authentication key containing <em>key</em>, <em>secret</em> and <em>region</em> keys.'),
        '#element_validate' => [$this, 'validateKey'],
      ],
    ];
    if (array_key_exists('aws', $options)) {
      $form['key_name']['#default_value'] = 'aws';
    }
    $schemeName = $this->fieldDefinition->getSetting('uri_scheme');
    $form['bucket'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('S3 Bucket'),
      '#description' => $this->t('Bucket to send uploads directly.')
    ];
    if ($this->flySystemFactory && in_array($schemeName, $this->flySystemFactory->getSchemes())) {
      $fileSystem = $this->flySystemFactory->getSettings($schemeName);
      if ($fileSystem['driver'] === 's3') {
        $form['bucket'] = [
          '#default_value' => $fileSystem['config']['bucket'],
          '#disabled' => TRUE,
          '#description' => $this->t('Configuration automatically loaded from Flysystem.')
        ] + $form['bucket'];
      }
    }
    return $form;
  }

  public static function defaultSettings() {
    return [
      'key_name' => '',
      'bucket' => '',
    ];
  }

  public function settingsSummary() {
    $bucket = $this->getSetting('bucket');
    $key = $this->getSetting('key_name');
    $summary = [];
    $summary[] = $bucket
      ? $this->t('S3 Bucket: @bucket', ['@bucket' => $bucket])
      : $this->t('Must set S3 Bucket.');
    $summary[] = $key
      ? $this->t('Signing Key name: @key', ['@key' => $key])
      : $this->t('Must set signing key.');
    return $summary;
  }

  /**
   * Validator for key.
   */
  public function validateKey(&$element, FormStateInterface $form_state, &$complete_form) {
    $keyName = $element['#value'];
    $value = $this->keyRepository->getKey($keyName)->getKeyValues();
    if ($missing = array_diff(['key', 'secret', 'region'], array_keys($value))) {
      $form_state->setError(
        $element,
        $this->t('Key(s) @keys missing from Key', ['@keys', implode(', ', $missing)])
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $allowedExtensions = explode(
      ' ',
      $this->fieldDefinition->getSetting('file_extensions')
    );
    $this->elementProcessor->processElement(
      $element,
      $allowedExtensions,
      $this->getClient(),
      $this->getSetting('bucket')
    );
    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // This method could be called multiple times during form submission.
    // Since we are creating file entities here, note whether we have already
    // done the work and return as such.
    $storageKey = $this->fieldDefinition->getName() . '_massage';
    if ($new_values = $form_state->get($storageKey)) {
      return $new_values;
    }
    $uploaded = $this->elementProcessor->processPostedFiles(
      reset($values)['files'],
      $this->getDestinationSchemeAndPrefix(),
      $this->getClient(),
      $this->getSetting('bucket')
    );
    foreach ($uploaded as $uri) {
      $created = File::create([
        'uri' => $uri,
        // This remote file is not temporary like the default file widget.
        // @todo Address cleanup?
        'status' => 1,
        'uid' => $this->currentUser->id(),
      ]);
      $created->save();
      $new_values[] = ['target_id' => $created->id()];
    }
    $form_state->set($storageKey, $new_values);
    return $new_values;
  }

  /**
   * Get a destination scheme and prefix for constructing a URI.
   *
   * @return string
   *   Destination per field settings, e.g. uri_scheme://[file_directory/]
   */
  protected function getDestinationSchemeAndPrefix(): string {
    $directory = $this->fieldDefinition->getSetting('file_directory');
    return $this->fieldDefinition->getFieldStorageDefinition()->getSetting('uri_scheme')
      . '://'
      . ($directory ? ($directory . '/') : '');
  }

  /**
   * Return a credential provider.
   *
   * @see https://docs.aws.amazon.com/aws-sdk-php/v3/guide/guide/credentials.html#creating-a-custom-provider
   *
   * @return \Closure|\GuzzleHttp\Promise\RejectedPromise
   */
  protected function getAwsCredentialsProvider() {
    $keyName = $this->getSetting('key_name');
    return function () use ($keyName) {
      $config = $this->keyRepository->getKey($keyName)->getKeyValues();
      if ($config['key'] && $config['secret']) {
        return promise_for(
          new Credentials($config['key'], $config['secret'])
        );
      }

      $msg = sprintf(
        'Could not load AWS credentials from Drupal key %s',
        $keyName
      );
      return new RejectedPromise(new CredentialsException($msg));
    };
  }

  /**
   * Get an S3 client.
   *
   * @return \Aws\S3\S3ClientInterface
   */
  protected function getClient(): S3ClientInterface {
    return new S3Client([
      'region' => $this->keyRepository
        ->getKey($this->getSetting('key_name'))->getKeyValues()['region'],
      'version' => '2006-03-01',
      'credentials' => $this->getAwsCredentialsProvider(),
    ]);
  }

}
